var form = document.getElementById('applyform');
form.addEventListener('submit', verifyFields);



function verifyFields(e) {
    e.preventDefault();
    var firstName = document.getElementById("firstName").value;
    var lastName = document.getElementById("lastName").value;
    var email = document.getElementById("email").value;
    var username = document.getElementById("username").value;
    var password = document.getElementById("password").value;
    var confirmPassword = document.getElementById("confirm_password").value;
    var aboutYourSelf = document.getElementById("about").value;
    var error = document.getElementById("error");
    var message = '';
    if (validateNotEmpty(firstName) && validateNotEmpty(lastName) && email && username && password
        && confirmPassword && aboutYourSelf) {
            if (password != confirmPassword){
                message = "Your password and confirmation password do not match";
            } else {
        message = "Your registration has been submitted";
            }
    } if (!validateNotEmpty(firstName)) {
        message = "First name should not be empty";
    } if (!validateNotEmpty(lastName)) {
        message = "Last name should not be empty";
    } if (!email) {
        message = "E-mail shoud not be empty";
    } if (!username) {
        message = "Username shoud not be empty";
    } if (!password) {
        message = "Password shoud not be empty";
    } if (!confirmPassword) {
        message = "Confirm password shoud not be empty";
    } if (!aboutYourSelf) {
        message = "About yourself shoud not be empty";
    }

    displayValues(error, message)
}

function displayValues(element, message) {
    element.innerHTML = message;
}

function validateNotEmpty(element) {
    if (element == '' || element == null) {
        return false;
    } else
        return true;


}