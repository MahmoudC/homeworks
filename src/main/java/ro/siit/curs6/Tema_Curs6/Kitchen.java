package ro.siit.curs6.Tema_Curs6;

public class Kitchen extends Room {

    public String coffeMachine;
    public String waterDispenser;
    public String fridge;

    public Kitchen(String furniture,String appliance) {
        super(furniture, appliance);
    }
}
