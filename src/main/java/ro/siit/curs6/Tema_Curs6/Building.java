package ro.siit.curs6.Tema_Curs6;

import java.util.ArrayList;
import java.util.Map;
import java.util.TreeMap;

public class Building {

    private final TreeMap<Floor, ArrayList<Room>> floors = new TreeMap<>();


    public void addFloor() {
        int i = 1;
        Floor floor = new Floor(i);
        System.out.println("Floor number " + i + " was added");
    }

}
