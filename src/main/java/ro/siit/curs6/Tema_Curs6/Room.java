package ro.siit.curs6.Tema_Curs6;

public abstract class Room {

    public String furniture;
    public String appliance;

    public Room(String furniture, String appliance) {
        this.furniture = furniture;
        this.appliance = appliance;
    }

}
