package ro.siit.curs4_5;

import java.awt.*;

public class Car {
    private float fuelLevel;
    private byte gear;
    private byte maxGear;
    private float speed;
    private float maxSpeed;
    private Color color;
    private boolean engineRunning;

    private static int nrOfCars;

    public static int getNrOfCars() {
        return nrOfCars;
    }

    public static void setNrOfCars(int nrOfCars) {
        Car.nrOfCars = nrOfCars;
    }


    public Car(Color color, byte maxGear, float maxSpeed) {
        this.color = color;
        this.maxGear = maxGear;
        this.maxSpeed = maxSpeed;
        Car.nrOfCars++;
    }

    public float getFuelLevel() {
        return fuelLevel;
    }

    public void setFuelLevel(float fuelLevel) {
        this.fuelLevel = fuelLevel;
    }

    public byte getGear() {
        return gear;
    }

    public void setGear(byte gear) {
        this.gear = gear;
    }

    public float getSpeed() {
        return speed;
    }

    public void setSpeed(float speed) {
        this.speed = speed;
    }

    public float getMaxSpeed() {
        return maxSpeed;
    }

    public void setMaxSpeed(float maxSpeed) {
        this.maxSpeed = maxSpeed;
    }

    public Color getColor() {
        return color;
    }

    public void setColor(Color color) {
        this.color = color;
    }

    public boolean isEngineRunning() {
        return engineRunning;
    }

    public void setEngineRunning(boolean engineRunning) {
        this.engineRunning = engineRunning;
    }


    public void setMaxGear(byte maxGear) {
        this.maxGear = maxGear;
    }

    public byte getMaxGear() {
        return maxGear;
    }

    public void accelerateSpeed(float speedDelta) {
        speed = speed + speedDelta;
        if (speed > maxSpeed)
            speed = maxSpeed;
    }

    public void accelerate() {
        accelerateSpeed(10);
        if (speed == 0)
            this.gear = 0;

        if (speed > 0 && speed <= 20) {
            this.gear = 1;
        } else if (speed >= 21 && speed <= 50)
            this.gear = 2;
        else if (speed >= 51 && speed <= 101)
            this.gear = 3;
    }

    public void steer(float angle) {
    }

    public void gearUp() {
        if (gear < maxGear)
            this.gear++;
    }

    public void gearDown() {
        if (gear > -1)
            this.gear--;
    }

    public void stop() {
        setEngineRunning(false);
        speed = 0;
    }

    public void start() {
        speed = 0;
        accelerate();
        setEngineRunning(true);
    }

    public static void horn() {
        System.out.println("Tit tit tit...");
    }

    @Override
    public String toString() {
        return "Car{" +
                "gear=" + gear +
                ", maxGear=" + maxGear +
                ", speed=" + speed +
                ", maxSpeed=" + maxSpeed +
                ", engineRunning=" + engineRunning +
                ", numberOfCars=" + getNrOfCars() +
                '}';
    }
}

