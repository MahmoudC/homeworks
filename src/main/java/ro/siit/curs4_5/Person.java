package ro.siit.curs4_5;

public class Person {
    String name;
    int age;
    boolean isHungry;

    void eat() {
        isHungry = false;
    }

    void printPerson() {
        System.out.println("Numele este: " + name);
        System.out.println("Varsta este " + age);
    }
}
