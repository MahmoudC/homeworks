package ro.siit.curs7;

public class MyCustomException extends IllegalArgumentException {

    public MyCustomException() {
        super();
    }

    public MyCustomException(String message) {
        super("Mesajul este: " + message);
    }

    public MyCustomException(String message, int errorCode) {
        super("Mesaj: " + message + " code eroare: " + errorCode);
    }

}
