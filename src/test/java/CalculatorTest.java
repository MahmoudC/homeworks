import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import ro.siit.curs7.Calculator;

public class CalculatorTest {

    static Calculator c;

    @BeforeClass
    public static void beforeTest(){
        c = new Calculator();
    }

    @Test
    public void testSum01(){
//        System.out.println(c.compute(2, 7, "+"));
        Assert.assertEquals(9, c.compute(2, 7, "+"), 0);
    }

    @Test
    public void testSum02(){
        Assert.assertEquals(-13482, c.compute(2164, -15646, "+"), 0);
    }

    @Test
    public void testSum03(){
        Assert.assertEquals(1000, c.compute(1000, 0, "+"), 0);
    }

    @Test()
    public void testSum04(){
        Assert.assertEquals(-100, c.compute(-1000, 900,"+"), 0);
    }

    @Test
    public void TestDiff01(){
        Assert.assertEquals(900,c.compute(1000, 100, "-"), 0);
    }

    @Test
    public void TestDiff02(){
        Assert.assertEquals(100, c.compute(50, -50, "-"), 0);
    }

    @Test
    public void TestDiff03(){
        Assert.assertEquals(-300, c.compute(-1000, -700, "-"), 0);
    }

    @Test
    public void TestDiff04(){
        Assert.assertEquals(-2800, c.compute(-2000, 800, "-"), 0);
    }

    @Test
    public void TestProduct01(){
        Assert.assertEquals(121, c.compute(11, 11, "*"), 0);
    }

    @Test
    public void TestProduct02(){
        Assert.assertEquals(-250, c.compute(125, -2, "*"), 0);
    }

    @Test
    public void TestProduct03(){
        Assert.assertEquals(250, c.compute(-125, -2, "*"), 0);
    }

    @Test
    public void TestProduct04(){
        Assert.assertEquals(10.24, c.compute(5.1234, 2, "*"), 0.01);
    }

    @Test(expected = IllegalArgumentException.class)
    public void TestUsupp01(){
        Assert.assertEquals(121, c.compute(11, 11, "x"), 0);
    }

    @Test
    public void TestDiv01(){
        Assert.assertEquals(2, c.compute(10, 5, "/"), 0);
    }

    @Test(expected = IllegalArgumentException.class)
    public void TestUnsupp02(){
        Assert.assertEquals(4, c.compute(654654, 3, "%"), 0);
    }

    @Test
    public void TestDiv02(){
        Assert.assertEquals(3.33, c.compute(10, 3, "/"), 0.01);
    }

    // This is the correct way to test if an exception is expedted to be raised
    @Test(expected = IllegalArgumentException.class)
    public void TestDiv03(){
        Assert.assertEquals(0,c.compute(10, 0, "/"), 0);
    }

    @Test
    public void TestDiv04(){
        Assert.assertEquals(-5, c.compute(-300, 60, "/"), 0);
    }

    @Test
    public void TestDiv05(){
        Assert.assertEquals(4, c.compute(-2000, -500, "/"), 0);
    }

    @Test
    public void TestDiv06(){
        Assert.assertEquals(-60, c.compute(240, -4, "/"), 0);
    }

    // This is just to catch exception and the test will be passed eitherway, please do not use this example !!
//    @Test
//    public void TestDiv04(){
//        try {
//            Assert.assertEquals(0, c.compute(10, 0, "/"), 0);
//        }
//        catch (Exception e) {
//            System.out.println(e.getMessage());
//        }
//    }

    @Test
    public void TestSqrt01(){
        Assert.assertEquals(1.4142, c.compute(2, 0, "SQRT"), 0.001);
    }

    @Test(expected = AssertionError.class)
    public void TestSqrt02(){
        Assert.assertEquals(0, c.compute(-10, 0, "SQRT"), 0);
    }

    @Test(expected = AssertionError.class)
    public void TestSqrt03(){
        Assert.assertEquals(-3, c.compute(9, 0, "SQRT"), 0);
    }

    @Test
    public void TestSqrt04(){
        Assert.assertEquals(0, c.compute(0, 0, "SQRT"), 0);
    }

    @Test(expected = IllegalArgumentException.class)
    public void TestUnsuppSQRT(){
        Assert.assertEquals(123, c.compute(100, 0, "sqrt"), 0);
    }

}
