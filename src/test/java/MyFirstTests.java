import org.junit.*;

public class MyFirstTests {

    @BeforeClass
    public static void beforeClass(){
        System.out.println("--> This runs before all tests in class");
    }

    @Before
    public void beforeTest(){
        System.out.println("- This runs before each test");
    }

    @Test
    public void test01() {
        System.out.println("This is my first test !!");
    }

    @Test
    public void test02() {
        System.out.println("Second test !!!");
    }

    @Ignore
    public void ignoredTest(){
        System.out.println("This is ignored!");
        String a = "abc";
        String b = "abc"; // a si b sunt obiecte diferite, cu aceeasi valoare

        String c = "cde";
        String d = c; // c, d sunt referinta --> acelas obiect cu valoarea respectiva
    }

    @After
    public void afterTest(){
        System.out.println("+ This runs after test");
    }

    @AfterClass
    public static void afterClass(){
        System.out.println("--> This runs after all tests in class");
    }

}
